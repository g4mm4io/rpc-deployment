# rpc-deployment
## Run your own Pulsechain RPC Endpoint
### Server requirements
- OS: Ubuntu 22.04 LTS
- Disk: >= 3 TB; SSD
- RAM: >= 32 GB

This playbook is tested against Ubuntu 22.04 LTS. Other Ubuntu versions 
probably work. This will not work on Windows, or Mac. You'll need to take 
these instructions and modify them to work on other systems.

**NOTICE!!!!
This is a very simple installation with one server. For a more
complicated setup you'll need to learn ansible, and configure.**

You will install erigon < execution > and lighthouse < consensus> using 
docker.

If you need to update Erigon or Lighthouse, you just re-run the ansible
command. It will pull the latest image, and update.

### Install SSL Certs and DNS.
You'll need to get SSL certificates, and configure the nginx config file server.conf.j2
Change server.conf.j2 to add SSL certs, enable https

Use the link to get certs. 
https://www.mortysnode.nl/secure-your-strong-node-with-a-reverse-proxy/

You'll need to set an A record with subdomain.example.com 1.2.3.4.

## Ansible
### Configure Ansible
1. Copy example file: inventory.example > inventory
2. Copy example file: ansible.cfg.example > ansible.cfg ( Review and changes as needed )
3. Copy example file: install_server.yml.example > install_server.yml ( review notes, and change as needed )
4. Copy example file: server.conf.j2.example > server.conf.j2 ( No changes needed for http )

### Install
https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

### Run

`ansible-playbook playbooks/install_server.yml` ( look at the generated output and 
fix any issues )

Note: make sure you've added the path to ssh keys, and your ip address. 

## Docker Help
https://docs.docker.com/engine/reference/commandline/logs/

container name is either "consensus" or "execution".

`sudo docker logs --follow <containername>` show logs

`sudo docker ps` running containers

`sudo docker stop -t 180 <containername>` stop container

`sudo docker rm <container>` remove container


# Socials
- Twitter: https://www.twitter.com/g4mm4io
- Telegram Chat: https://t.me/g4mm4ioChat
- Website: www.g4mm4.io

# Disclaimer
The information provided by g4mm4io ("we," "us," or "our") is for general 
informational purposes only. All information is provided in good faith, 
however we make no representation or warranty of any kind, express or 
implied, regarding the accuracy, adequacy, validity, reliability, availability, 
or completeness of any information. UNDER NO CIRCUMSTANCE SHALL WE HAVE ANY 
LIABILITY TO YOU FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE 
OR RELIANCE ON ANY INFORMATION PROVIDED. YOUR USE AND YOUR RELIANCE ON ANY 
INFORMATION IS SOLELY AT YOUR OWN RISK.


